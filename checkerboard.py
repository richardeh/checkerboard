"""
Checkerboard.py
Creates a checkerboard of height H and width W (in pixels)
using colors [colors]

Author: Richard Harrington
Date Created: 9/16/2013
Last Modified: 9/17/2013
"""
from tkinter import *
from math import floor

class Checkerboard():
    H,W=0,0
    colors=[]
    def __init__(self,height,width,color1,color2):
        # Accepts height, width, and two color values
        self.H,self.W=height,width
        self.colors.append(color1)
        self.colors.append(color2)
        
        master=Tk()

        c=Canvas(master, width=self.W,height=self.H)
        c.pack()
    
        for x in range(0,8):
            for y in range(0,8):
                x_start=floor(x*self.W/8)
                y_start=floor(y*self.H/8)
                x_stop=floor((x+1)*self.W/8)
                y_stop=floor((y+1)*self.W/8)
                m=lambda x:x%2
                if (m(y)and m(x)) or (not m(y) and not m(x)):
                    color=self.colors[0]
                else:
                    color=self.colors[1]
                c.create_rectangle(x_start,y_start,x_stop,y_stop,fill=color)
        master.bind("<Button-1>", self.callback)

        mainloop()

    def callback(self,event):
        # Converts the X,Y pixel locations of the event to a Column,Row position
        print(self.px_to_column(event.x,self.W),self.px_to_row(event.y,self.H))

    def px_to_column(self,x_val,w):
        # Converts an input value of X to one of eight columns
        pos=floor(1000*x_val/w)
        if pos in range (0,125):
            return "A"
        elif pos in range(125,250):
            return "B"
        elif pos in range(250,375):
            return "C"
        elif pos in range(375,500):
            return "D"
        elif pos in range(500,625):
            return "E"
        elif pos in range(625,750):
            return "F"
        elif pos in range(750,875):
            return "G"
        elif pos in range(876,1000):
            return "H"
        
    def px_to_row(self,y_val,h):
        # Converts an input value of Y to one of eight rows

        pos=floor(1000*y_val/h)
        if pos in range (0,125):
            return "1"
        elif pos in range(125,250):
            return "2"
        elif pos in range(250,375):
            return "3"
        elif pos in range(375,500):
            return "4"
        elif pos in range(500,625):
            return "5"
        elif pos in range(625,750):
            return "6"
        elif pos in range(750,875):
            return "7"
        elif pos in range(876,1000):
            return "8"
    

#Example usage:
#myCB=Checkerboard(640,640,"black","red")
